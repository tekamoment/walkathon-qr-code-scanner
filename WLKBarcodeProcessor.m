//
//  WLKBarcodeProcessor.m
//  Walkathon
//
//  Created by Carlos Arcenas on 8/11/13.
//  Copyright (c) 2013 Carlos Arcenas. All rights reserved.
//

#import "WLKBarcodeProcessor.h"
#import "Reachability.h"
#import <Parse/Parse.h>
@import SystemConfiguration;

#define kBarcodeDomainPrefix @"AISWalkathon"
//#define kBarcodeDomainPrefix @"http://ais-indonesia.com/walkathon/runner/"

NSString *kBarcodeProcessorQueueClearedNotification = @"kBarcodeProcessorQueueClearedNotification";

@interface WLKBarcodeProcessor ()
@property BOOL isNetworkAccessible;
@property (nonatomic, strong) NSMutableArray *queuedIDsArray;
@property (nonatomic, strong) Reachability *internetReachability;
@end

@implementation WLKBarcodeProcessor

+(instancetype)barcodeProcessor {
    static WLKBarcodeProcessor *processor;
    static dispatch_once_t onceToken;
    @synchronized(self){
        dispatch_once(&onceToken, ^{
            processor = [[WLKBarcodeProcessor alloc] init];
            [[NSNotificationCenter defaultCenter] addObserver:processor selector:@selector(checkForInternetConnection) name:kReachabilityChangedNotification object:nil];
            [processor checkForInternetConnection];
            processor.internetReachability = [Reachability reachabilityForInternetConnection];
            if ([processor.internetReachability startNotifier]) {
                NSLog(@"NOTIFIER HAS STARTED.");
            } else {
                NSLog(@"NOTIFIER DID NOT START.");
            }
        });
    }
    return processor;
}

-(void)checkForInternetConnection {
    NSLog(@"checking for internet connection");
    NetworkStatus internetStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
    if (internetStatus == NotReachable) {
        _isNetworkAccessible = NO;
    } else if ([[Reachability reachabilityForInternetConnection]  connectionRequired]){
        _isNetworkAccessible = NO;
    } else {
        if (_queuedIDsArray.count > 0) {
            [self sendQueueToParse];
        }
        _isNetworkAccessible = YES;
    }
}

-(void)processBarcodeWithStringValue:(NSString *)stringValue withCompletionHandler:(void (^)(bool, NSString *, int))completionHandler {
    NSUInteger prefixCount = kBarcodeDomainPrefix.length;
    if ([[stringValue substringToIndex:prefixCount] isEqualToString:kBarcodeDomainPrefix]) {
        NSString *raceNumber = [stringValue substringFromIndex:prefixCount];
        NSLog(@"It fits!, %@", [stringValue substringFromIndex:prefixCount]);
        NSDate *date = [NSDate date];
        
        // saving to parse
        // IMPORTANT NOTE: THIS DOES NOT YET INCLUDE PREVENTION OF MULTIPLE SCANS OF SAME CODE IN SUCCESSION.
        // INCORPORATE LAST SCANNED CODE
        
        /*
        if (_lastScanned == raceNumber) {
            completionHandler(false, [NSString stringWithFormat:@"Walker %@ has been scanned right before.", raceNumber], 1);
        }
         */

        // CRAP! DOESN'T WORK WITHOUT NETWORK ACCESS!!!
        
        if (_isNetworkAccessible == YES) {
            PFQuery *query = [PFQuery queryWithClassName:@"Walker"];
            [query whereKey:@"raceNumber" equalTo:raceNumber];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (!object) {
                    NSLog(@"Object doesn't exist!");
                    // pop up view
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(NO, nil, 0);
                    });
                } else {
                    NSLog(@"Object found! %@", object);
                    // [object addObject:date forKey:@"runTimes"];
                    [object incrementKey:@"lapsRun"];
                    NSNumber *lapsRunFromParse = (NSNumber *)[object objectForKey:@"lapsRun"];
                    __block int lapsRun = [lapsRunFromParse integerValue];
                    //NSLog(@"Laps run: %@, %d, %@", lapsRunFromParse, lapsRun, [lapsRun class]);
                    NSLog(@"Laps run: %d", lapsRun);
                    [object setObject:date forKey:@"lastScannedDate"];
                    [object saveEventually:^(BOOL succeeded, NSError *error) {
                        NSLog(@"Save succeeded! Passing in: %d, %@, %d.", succeeded, raceNumber, lapsRun);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completionHandler(succeeded, raceNumber, lapsRun);
                        });
                    }];
                }
            }];
        } else {
            // TODO: add the scanned id to a queue for processing after
            if (!_queuedIDsArray) {
                _queuedIDsArray = [NSMutableArray array];
            }
            [_queuedIDsArray addObject:raceNumber];
            // save to nsuserdefaults
            NSLog(@"Queued IDs array contents: %@", _queuedIDsArray);
            NSLog(@"Not available; queueing Race Number %@", raceNumber);
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(NO, raceNumber, 0);
            });
        }
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(NO, nil, 0);
        });
    }
}

-(void)saveQueuedArrayToDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_queuedIDsArray forKey:@"queuedIDsArray"];
    [defaults synchronize];
}

-(void)sendQueueToParse {
    // send each stuff to parse here
    // check for writing out to plists
    
    // If the network is working
        // Follow things established in the processBarcodeWithString: method
    
    
    // Else the network is not working
        // stop.
    
    // Somehow manage to record each object that has been sent over
    // possibly do this in background?
    
    // Array to hold each object that has been saved.
    __block NSMutableArray *sentCodes = [NSMutableArray arrayWithCapacity:_queuedIDsArray.count];
    __block int codesRemaining = [_queuedIDsArray count];
    
    [_queuedIDsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *raceNumber = (NSString *)obj;
        
        if (_isNetworkAccessible == YES) {
            PFQuery *query = [PFQuery queryWithClassName:@"Walker"];
            [query whereKey:@"raceNumber" equalTo:raceNumber];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (!object) {
                    NSLog(@"Object doesn't exist!");
                    // pop up view
                } else {
                    NSLog(@"Object found! %@", object);
                    // [object addObject:date forKey:@"runTimes"];
                    [object incrementKey:@"lapsRun"];
                    NSNumber *lapsRunFromParse = (NSNumber *)[object objectForKey:@"lapsRun"];
                    __block int lapsRun = [lapsRunFromParse integerValue];
                    //NSLog(@"Laps run: %@, %d, %@", lapsRunFromParse, lapsRun, [lapsRun class]);
                    NSLog(@"Laps run: %d", lapsRun);
                    [object setObject:[NSDate date] forKey:@"lastScannedDate"];
                    [object saveEventually:^(BOOL succeeded, NSError *error) {
                        if (succeeded == YES) {
                            NSLog(@"Save succeeded! Passing in: %d, %@, %d.", succeeded, raceNumber, lapsRun);
                            [sentCodes addObject:obj];
                            codesRemaining--;
                            if (codesRemaining == 0) {
                                [self queueSentToParseWithSentCodes:sentCodes];
                            }
                        }
                    }];
                }
            }];
        } else {
            // Since the Internet is not accessible, stop attempting to send objects over.
            *stop = YES;
            [self queueSentToParseWithSentCodes:sentCodes];
        }
    }];
}

-(void)queueSentToParseWithSentCodes:(NSArray *)codes {
    if (codes.count > 0) {
        for (NSString *code in codes) {
            NSLog(@"Removing code: %@ as it has been sent to parse", code);
            [_queuedIDsArray removeObjectIdenticalTo:code];
        }
    }
    
    if (_queuedIDsArray.count == 0) {
        // delegate to view controller to put banner up top.
        NSLog(@"ALL QUEUED CODES SCANNED!");
        [[NSNotificationCenter defaultCenter] postNotificationName:kBarcodeProcessorQueueClearedNotification object:nil];
    } else {
        NSLog(@"Remaining codes: %@", _queuedIDsArray);
    }
}

@end
