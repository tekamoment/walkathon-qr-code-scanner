//
//  WLKAppDelegate.h
//  Walkathon
//
//  Created by Carlos Arcenas on 8/11/13.
//  Copyright (c) 2013 Carlos Arcenas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
