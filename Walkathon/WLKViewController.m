//
//  WLKViewController.m
//  Walkathon
//
//  Created by Carlos Arcenas on 8/11/13.
//  Copyright (c) 2013 Carlos Arcenas. All rights reserved.
//

#import "WLKViewController.h"
#import "WLKSessionManager.h"
#import "WLKBarcodeProcessor.h"
#import "ALAlertBanner.h"
#import "CSNotificationView.h"
#import <AudioToolbox/AudioToolbox.h>

@interface WLKViewController () <UIGestureRecognizerDelegate>
// This view holds the Preview Layer and all other layers (e.g. HUDs)
@property (weak, nonatomic) IBOutlet UIView *previewView;
// This is the layer where the video stream of the camera resides.
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
// This is the object that handles all of the AV stuff. Read into it to understand more.
@property (strong, nonatomic) WLKSessionManager *sessionManager;
//This is a timer that goes off every 0.15 sec to check if there are any barcodes present. Might be deleted if NSNotifications are a better option.
@property (strong, nonatomic) NSTimer *barcodeTimer;
// Holds previous scanned code. Used to ensure succession scanning does not happen.
@property (strong, nonatomic) NSString *previousCode;
@end

@implementation WLKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Initialize the SessionManager and tell it to start running and receiving stuff from camera.
    _sessionManager = [[WLKSessionManager alloc] init];
    [_sessionManager startRunning];
    
    // Initialize a PreviewLayer, set properties, and push to previewView
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_sessionManager.captureSession];
    // set previewLayer to take up full screen
    [previewLayer setFrame:_previewView.bounds];
    // tell layer to fill up bounds
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    // find out what this does (probably checks if we can support certain orientations)
    if ([[previewLayer connection] isVideoOrientationSupported]) {
        // this ensures that the video is in portrait
        [[previewLayer connection] setVideoOrientation:AVCaptureVideoOrientationPortrait];
    }
    // this adds the layer to the view.
    [_previewView.layer addSublayer:previewLayer];
    // still unsure what this does
    [_previewView.layer setMasksToBounds:YES];
    _previewLayer = previewLayer;
    
    // add the four corners here to indicate how big the code should be
    
    // add the button on lower right corner for manual code entry
    UIView *buttonPlaceholder = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_previewView.frame) - 40, CGRectGetMaxY(_previewView.frame) - 40, 20, 20)];
    buttonPlaceholder.backgroundColor = [UIColor blueColor];
    [_previewView addSubview:buttonPlaceholder];
    
    UIImageView *fourCorners = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fourcorner"]];
    fourCorners.frame = CGRectMake((CGRectGetMidX(_previewView.frame) / 2) - 55, (CGRectGetMidY(_previewView.frame) / 2), 275, 275);
    [_previewView addSubview:fourCorners];
    
    // Timer to periodically check session if barcodes have been detected. Change to use NSNotifications?
    // I see now why timers are used. To prevent reading and reading and reading quickly. 1 second pause used here. 
    _barcodeTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(barcodeDetecting) userInfo:nil repeats:YES];
    
    // Register for the Queue Cleared Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queueCleared) name:kBarcodeProcessorQueueClearedNotification object:nil];
    
}

-(void)queueCleared {
    [CSNotificationView showInViewController:self
                                       style:CSNotificationViewStyleSuccess
                                     message:@"Code queue has been cleared!"];
}

- (void)barcodeDetecting {
    // Ensures that only one code is being scanned at a time.
    if (_sessionManager.barcodes.count > 1) {
        [CSNotificationView showInViewController:self
                                           style:CSNotificationViewStyleError message:@"Too many codes at one time!"];
        return;
    } else if (_sessionManager.barcodes.count < 1) {
        return;
    }
    
    @synchronized(_sessionManager) {
        // Fetches the only barcode detected.
        AVMetadataMachineReadableCodeObject *barcode = _sessionManager.barcodes.firstObject;
        
        // Causes device to vibrate.
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        NSLog(@"%@", barcode.stringValue);
        
        // The below lines are to be used when simply scanning for the IDs, and for when Walkathon is not yet in full swing. To be commented out when preparing apps for Walkathon deployment.
        
        /* 
        // BEGIN CODE SCAN TESTS
        
        NSUInteger prefixCount = @"AISWalkathon".length;
        if ([[barcode.stringValue substringToIndex:prefixCount] isEqualToString:@"AISWalkathon"]) {
            // For now: just scanning for the values:
            
            NSString *runnerID = [barcode.stringValue substringFromIndex:prefixCount];
            if ([runnerID isEqualToString:_previousCode]) {
                [CSNotificationView showInViewController:self
                                                   style:CSNotificationViewStyleError
                                                 message:[NSString stringWithFormat:@"Code %@ has just been scanned.", runnerID]];
            } else {
                _previousCode = runnerID;
                NSString *fullMessage = [NSString stringWithFormat:@"Full code: %@", runnerID];
                [CSNotificationView showInViewController:self
                                                   style:CSNotificationViewStyleSuccess
                                                 message:fullMessage];
            }
            
        } else {
        // For now: just scanning for the values:
            [CSNotificationView showInViewController:self
                                           style:CSNotificationViewStyleSuccess
                                         message:barcode.stringValue];
        }
         
         // END CODE SCAN TESTS
         */
        
        // TRUE if code passes both prefix and previousCode checks.
        BOOL isCodeSendable;
        
        NSUInteger prefixCount = @"AISWalkathon".length;
        if ([[barcode.stringValue substringToIndex:prefixCount] isEqualToString:@"AISWalkathon"]) {
            // For now: just scanning for the values:
            
            NSString *runnerID = [barcode.stringValue substringFromIndex:prefixCount];
            if ([runnerID isEqualToString:_previousCode]) {
                isCodeSendable = NO;
                [CSNotificationView showInViewController:self
                                                   style:CSNotificationViewStyleError
                                                 message:[NSString stringWithFormat:@"Code %@ has just been scanned.", runnerID]];
            } else {
                isCodeSendable = YES;
                _previousCode = runnerID;
                // send to model for processing. should be asynchronous.
                [[WLKBarcodeProcessor barcodeProcessor] processBarcodeWithStringValue:barcode.stringValue withCompletionHandler:^(bool success, NSString *scannedID, int lapsRun) {
                    NSString *bannerText;
                    if (success) {
                        bannerText = [NSString stringWithFormat:@"Lap for %@ sent! %d laps completed.", scannedID, lapsRun];
                    } else {
                        if (scannedID != nil) {
                            bannerText = [NSString stringWithFormat:@"Lap for %@ cached!", scannedID];
                        } else {
                            bannerText = @"Runner does not exist!";
                        }
                    }
                    
                    // This creates the banner to be shown at the top of the view when scanning is complete.
                    [CSNotificationView showInViewController:self
                                                       style:CSNotificationViewStyleSuccess
                                                     message:bannerText];
                }];
                
            }
            
        } else {
            // For now: just scanning for the values:
            isCodeSendable = NO;
            [CSNotificationView showInViewController:self
                                               style:CSNotificationViewStyleError
                                             message:@"Invalid code."];
        }
        
        
        
        // This flashes a green screen when the code is scanned.
        // Helps show the user that a scan is successful.
        if (isCodeSendable == YES) {
            UIView *flashView = [[UIView alloc] initWithFrame:_previewLayer.bounds];
            flashView.backgroundColor = [UIColor colorWithRed:0.21 green:0.72 blue:0.00 alpha:1.0];
            [_previewView addSubview:flashView];
            [UIView animateWithDuration:1.0 animations:^{
                flashView.alpha = 0.0;
            } completion:^(BOOL finished) {
                [flashView removeFromSuperview];
            }];
        } else {
            UIView *flashFalseView = [[UIView alloc] initWithFrame:_previewLayer.bounds];
            flashFalseView.backgroundColor = [UIColor redColor];
            [_previewView addSubview:flashFalseView];
            [UIView animateWithDuration:1.0 animations:^{
                flashFalseView.alpha = 0.0;
            } completion:^(BOOL finished) {
                [flashFalseView removeFromSuperview];
            }];
        }
        
    }
}
                     

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
