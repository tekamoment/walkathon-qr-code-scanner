//
//  main.m
//  Walkathon
//
//  Created by Carlos Arcenas on 8/11/13.
//  Copyright (c) 2013 Carlos Arcenas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WLKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WLKAppDelegate class]));
    }
}
