//
//  WLKBarcodeProcessor.h
//  Walkathon
//
//  Created by Carlos Arcenas on 8/11/13.
//  Copyright (c) 2013 Carlos Arcenas. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *kBarcodeProcessorQueueClearedNotification;

@interface WLKBarcodeProcessor : NSObject

+(instancetype)barcodeProcessor;
-(void)processBarcodeWithStringValue:(NSString *)stringValue withCompletionHandler:(void(^)(bool success, NSString * scannedID, int lapsCompleted))completionHandler;

@end
